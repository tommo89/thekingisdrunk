//
//  OneAndOneGame.h
//  Future Tactics
//
//  Created by Tommaso Checchi on 8/9/11.
//  Copyright 2011 none. All rights reserved.
//

#ifndef MonsterGame_h
#define MonsterGame_h

#include "monster_common_header.h"

namespace Monster
{
	class Level;
	class Intro;

	class MonsterGame : 
		public Dojo::Game, 
		public ResourceGroup,
		public Dojo::InputSystem::Listener,
		public Dojo::InputDevice::Listener
	{
	public:

		typedef Array< InputDevice* > DeviceList;

		bool debugOverlay;
		
		typedef const std::set< Dojo::String > GroupSet;
				
		MonsterGame();

		~MonsterGame();

		void reset();

		const DeviceList& getActiveDevices()
		{
			return activeDevices;
		}

		virtual void onDeviceConnected( Dojo::InputDevice* j );
		virtual void onDeviceDisconnected( Dojo::InputDevice* j );

		virtual void onButtonPressed( Dojo::InputDevice* j, int button );

		virtual void onApplicationFocusLost()
		{

		}

		virtual void onApplicationFocusGained()
		{

		}

		
	protected:
		Level* currentLevel;
		Intro* currentIntro;
		DeviceList activeDevices;
		
		virtual void onStateBegin();
		virtual void onStateLoop( float dt );
		virtual void onStateEnd();
		
		virtual void onBegin();
		virtual void onLoop( float dt );
		virtual void onEnd();
	};
}

#endif
