#ifndef Item_h__
#define Item_h__

#include "monster_common_header.h"
#define GRAVITY 4.f
#define PLAYER_HEIGHT 0.6f

#include "Particle.h"

namespace Monster
{
	class Item : 
		public Actor,
		public StateInterface
	{
	public:

		enum State
		{
			IS_IDLE,
			IS_PICKED_UP,
			IS_THROWN,
			IS_DESTROYED_ATTRACTING,
			IS_DESTROYED,
			IS_FALLING
		};

		Item( Level* level, const Vector& pos, const String& sprite  ) :
			Actor( level, pos, sprite ),
			rotationSpeed( 0 )
		{
			mLevel = level;
			shadow = new Sprite(level, position - Vector( 0, getHalfSize().y), "shadow");
			shadow->pixelScale = 0.8;
			/*shadow->srcBlend = GL_DST_COLOR;
			shadow->destBlend = GL_ZERO;*/
			level->addChild(shadow, Level::LL_SHADOWS );

			setState( IS_IDLE );
		}

		virtual ~Item()
		{

		}

		virtual void onThrow( const Vector& launchSpeed )
		{
			if( isCurrentState( IS_PICKED_UP ) )
			{
				speed = launchSpeed * 0.8f;

				shadow->speed = speed;

				speed.y += 1; //fake a drop!

				setState( IS_THROWN );
			}
		}

		virtual void onDrop()
		{
			if( isCurrentState( IS_PICKED_UP ) )
			{	
				position.y -= PLAYER_HEIGHT; //drop on ground
				setState( IS_IDLE );
			}
		}

		virtual bool onPickup()
		{
			if( isCurrentState( IS_IDLE ) )
			{
				setState( IS_PICKED_UP );
				return true;
			}
			else return true;
		}

		bool canPickUp()
		{
			return isCurrentState( IS_IDLE );
		}

		virtual void onStateBegin()
		{
			if( isCurrentState( IS_IDLE ) )
			{
				speed = 0;
				shadow->speed = 0;
				rotation = Quaternion();			
			}
			else if( isCurrentState( IS_PICKED_UP ) )
			{
			
			}
			else if( isCurrentState( IS_THROWN ) )
			{
				timer = 1.1;

				playerPos = position;
				rotationSpeed = Math::rangeRandom(10,50);
			}
			else if( isCurrentState( IS_FALLING ) )
			{
				shadow->setVisible( false );
				startFade( Color::WHITE, Color::BLACK, 1 );
				timer = 0;
				speed *= 0.1;
			}
			else if( isCurrentState( IS_DESTROYED_ATTRACTING ) )
			{
				setVisible( false );
				shadow->setVisible( false );
				timer = 0;
			}
			else if( isCurrentState( IS_DESTROYED ) )
			{
				shadow->dispose = dispose = true;
			}
		}

		virtual void onStateLoop( float dt )
		{
			//if the superclass set a next state, skip default behavior
			if( hasNextState() )
				return;

			if( isCurrentState( IS_THROWN ) )
			{
				rotation = Quaternion( Vector(0,0,timer*rotationSpeed) );
				timer += dt;
				speed.y -= GRAVITY * dt;

				int centerTile;
				Vector dir;
				//use the shadow to trace where the object is on the ground
				int hitTile = mLevel->collidesActor( this, shadow->getWorldMax(), shadow->getWorldMin(), dir.x, dir.y, centerTile );

				//hit a wall or the ground
				if( hitTile == 1 || shadow->position.distance( position ) < PLAYER_HEIGHT )
				{
					//set idle if there's solid ground, or falling if there's a pit
					if( centerTile == 3 )
						setState( IS_FALLING );
					else
					{
						Platform::getSingleton()->getSoundManager()->playSound( mLevel->getSound("object") );

						position = shadow->position + Vector( 0, getHalfSize().y * 0.3 );
						Particle::emit(15, shadow->position, mLevel);
						setState( IS_IDLE );
					}
				}
			}
			else if( isCurrentState( IS_PICKED_UP) )
			{
				shadow->position = position - Vector( 0, getHalfSize().y * 0.3 + PLAYER_HEIGHT );
			}
			else if( isCurrentState( IS_IDLE ) )
			{
				shadow->position = position - Vector( 0, getHalfSize().y * 0.3 );
			}
			else if( isCurrentState( IS_FALLING ) )
			{
				pixelScale -= 1.f * dt;
				rotation = Quaternion( Vector(0,0, timer*20.f) ); 
				timer += dt;
				if( !isFading() )
					setState( IS_DESTROYED );
			}
		}

		virtual void onStateEnd()
		{

		}

		virtual void onAction( float dt )
		{
			loop( dt );

			Sprite::onAction( dt );
		}

	protected:

		float rotationSpeed;
		double timer;

		Vector playerPos;
		Sprite* shadow;
		Level* mLevel;

	private:
	};
}

#endif // Item_h__
