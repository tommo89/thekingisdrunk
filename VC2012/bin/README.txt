The Goblin King is drunk and he's lost in his dungeon again... Please fellow minions, help him find a bed to crash for the night! Taunt him so he will follow you but beware his anger. Only a beer will calm him down!

This is a game made under 48h for the Global Game Jam 2013!
It's a game for 2 to 5 players! It supports single player too, but it's a lot better with more than one player so grab a friend or two (or more!) :-)
Supports keyboard and xbox 360 gamepads! (sorry, no other kind of pad support, time was against us :-( but if you have a ps3 pad you can use DS3Tool! )

features:

- procedural levels!
- fancy graphics!
- colored goblins!
- METAL! \m/_
- a very douchebaggy king.

== CREDITS ==
Programming:
Tommaso Checchi
Daniele Calleri

Art:
Giuseppe Navarria
Flaminia Grimaldi

Music:
Kevin MacLeod (incompetech.com)
Cannibal Corpse

Made @ Global Game Jam 2013 Rome - www.indievault.it
Game powered by the Dojo Engine: https://bitbucket.org/tommo89/dojo/src