#ifndef Saw_h__
#define Saw_h__

#include "monster_common_header.h"

#include "Actor.h"
#include "Level.h"
#include "Player.h"

#define SAW_STOP_DELAY 1.f
#define SAW_SPEED 10.f


namespace Monster
{
	class Saw : 
		public Actor,
		public StateInterface
	{
	public:

		enum State
		{
			SS_STOP_LEFT,
			SS_GOING_LEFT,
			SS_STOP_RIGHT,
			SS_GOING_RIGHT
		};

		Saw( Level* level, const Vector& pos ) :
		Actor( level, pos, "saw_rotate", 0.05 )
		{
			//move to the left position
			float tileSide = level->getTileSide();
			trackMax = position.x + tileSide * 2.5;
			trackMin = position.x - tileSide * 2.5;

			track = new Renderable( level, pos );
			track->setMesh( level->getMesh("texturedQuad") );
			track->scale = Vector( tileSide * 6.2, 0.1 );
			track->color = 0xff22211b;
			level->addChild( track, Level::LL_SHADOWS );

			setState( SS_STOP_LEFT );
		}

		virtual void onStateBegin()
		{
			if( isCurrentState( SS_STOP_LEFT ) )
			{
				timer = 0;
				position.x = trackMin;
				speed.x = 0;
			}
			else if( isCurrentState( SS_GOING_LEFT ) )
			{
				speed.x = -SAW_SPEED;
			}
			else if( isCurrentState( SS_STOP_RIGHT ) )
			{
				timer = 0;
				position.x = trackMax;
				speed.x = 0;
			}
			else if( isCurrentState( SS_GOING_RIGHT ) )
			{
				speed.x = SAW_SPEED;
			}
		}

		virtual void onStateLoop( float dt )
		{
			Level* level = (Level*)getGameState();

			//kill stuff
			Monster* m = level->getMonster();
			if( m->getPivot().distance( position ) < 0.7 )
				m->onHit();
			
			auto players = level->getPlayers();
			for( auto player: players )
			{
				if( player->getPivot().distance( position ) < 0.7 )
					player->_damage();
			}

			if( isCurrentState( SS_STOP_LEFT ) )
			{
				timer += dt;
				if( timer > SAW_STOP_DELAY )
					setState( SS_GOING_RIGHT );
			}
			else if( isCurrentState( SS_GOING_LEFT ) )
			{
				if( position.x < trackMin )
					setState( SS_STOP_LEFT );
			}
			else if( isCurrentState( SS_STOP_RIGHT ) )
			{
				timer += dt;
				if( timer > SAW_STOP_DELAY )
					setState( SS_GOING_LEFT );
			}
			else if( isCurrentState( SS_GOING_RIGHT ) )
			{
				if( position.x > trackMax )
					setState( SS_STOP_RIGHT );
			}
		}

		virtual void onStateEnd()
		{

		}

		virtual void onAction(  float dt )
		{
			loop( dt );
			Actor::onAction(dt);
		}
		
	protected:

		float trackMax, trackMin;

		Dojo::Renderable* track;
		double timer;
	private:
	};

}

#endif // Saw_h__
