#ifndef Monster_h__
#define Monster_h__

#include "monster_common_header.h"

#include "Actor.h"
#include "Particle.h"

#define MONSTER_OSCILLATION 3.0f
#define INFLUENCE_DISTANCE_MAX 7.f
#define ATTRACT_FORCE 3.f
#define SCARE_FORCE 2.f
#define MONSTER_ANGER_INCREMENT 2.0f
#define MONSTER_COOLDOWN (MONSTER_ANGER_INCREMENT * 0.2f)
#define ANGER_HIT_DECREASE 0.4f

namespace Monster
{

	class Monster : 
		public Actor,
		public Dojo::StateInterface
	{
	public:

		enum State
		{
			MS_WALKING,
			MS_HITTING,
			MS_DRINKING,
			MS_FALLING,
			MS_DEAD
		};

		Monster( Level* level, const Vector& pos ) :
			Actor( level, pos, "king_walk", 0.1 ),
			timer( 0 ),
			anger( 0 ),
			particleTimer( 0 ),
			hitTimer( 0 ),
			angryFaceTimer( 0 )
		{
			registerAnimation( "king_angry_walk", 0.1 );
			registerAnimation( "king_drink", 0.1 );

			noiser = new Noise( Random( 1 ) );

			mLevel = level;

			//aabb scaled
			aabb.max.x = getHalfSize().x * 0.7;
			aabb.max.y = 0.0f;

			aabb.min = -getHalfSize();
			aabb.min.x *= 0.7;

			base.max = aabb.max * 0.2f;
			base.min = aabb.min * 0.2f;

			setState( MS_WALKING );
		}

		void _addForce( const Vector& pos, bool offence, float f )
		{
			Vector dist = pos-position;

			float d = dist.length();
			if( d > INFLUENCE_DISTANCE_MAX )
				return;

			float s = 1.f - d / INFLUENCE_DISTANCE_MAX;
			Vector force = dist.normalized() * s * f;
			playerInfluence += force;

			totalPlayerPush += force.length();

			addAnger( offence ? 0.01 : -0.01 );
		}

		void addAnger( float f )
		{
			anger = Math::max( 0.f, f + anger );

			if( f > 0 )
				angryFaceTimer = 0.5;
		}

		float getAnger()
		{
			return anger;
		}

		virtual void onExplosion( const Vector& direction, float intensity )
		{
			onHit();
		}

		virtual void onAttract( const Vector& pos, bool offence, float m = 1 )
		{
			_addForce( pos, offence, ATTRACT_FORCE * m );
		}

		virtual void onPlayerScare( const Vector& pos )
		{
			_addForce( pos, -SCARE_FORCE, true );
		}

		virtual void onPlayerHit()
		{
			if( !isCurrentState( MS_HITTING ) )
				setState( MS_HITTING );
		}

		virtual void onHit()
		{
			if( hitTimer > 1 )
			{
				hitTimer = 0;
				addAnger( 0.8 );
				onPlayerScare( position + Math::randomVector2D( -3,3 ) );
			}
		}

		virtual void onAction( float dt )
		{
			loop( dt );

			Sprite::onAction( dt );
		}

	protected:

		Noise* noiser;
		double timer, hitTimer;
		Level* mLevel;

		float particleTimer;
		double angryFaceTimer;

		float anger;

		Vector playerInfluence;
		float totalPlayerPush;

		virtual void onStateBegin()
		{
			if( isCurrentState( MS_WALKING ) ) 
			{
				setAnimation( 0 );
			}
			else if( isCurrentState( MS_HITTING ) )
			{
				//todo animation
				setAnimation( 0 );
			}
			else if( isCurrentState( MS_DRINKING ) )
			{
				animationSpeedMultiplier = 1;
				speed = 0;
				setAnimation( 2 );
				Platform::getSingleton()->getSoundManager()->playSound(mLevel->getSound("birra"));
			}
			else if( isCurrentState( MS_FALLING ) )
			{
				Platform::getSingleton()->getSoundManager()->playSound( mLevel->getSound("kingfall") );

				setAnimation( 1 );
				startFade( Color::WHITE, Color::BLACK, 1 );
				timer = 0;
				speed *= 0.1;
			}
		}

		virtual void onStateLoop( float dt )
		{
			float dirX, dirY;
			int centerTileType;
			int tileType = mLevel->collidesActor(this, position+(aabb.max*0.7f), position+(aabb.min*0.7f), dirX, dirY, centerTileType );

			// set music volume to monster anger			
			float vol=anger;

			if(anger != anger) vol = 0.0f;
			if(vol < 0.0f) vol = 0.0f;

			mLevel->setMusicMix(vol);

			if( isCurrentState( MS_WALKING ) )
			{
				hitTimer += dt;

				if(tileType == 2)
				{
					anger += dt * MONSTER_ANGER_INCREMENT * 4.0f;
				}
				else if( centerTileType == 3 )
					setState( MS_FALLING );

				Vector randomDirection( 
					noiser->noise( timer, -timer, MONSTER_OSCILLATION ), 
					noiser->noise( timer, timer, MONSTER_OSCILLATION ) );

				timer += dt;

				float wrongDirectionFactor = 0;//(playerInfluence.normalized() * randomDirection.normalized()) * 0.1f;

				if( totalPlayerPush - playerInfluence.length() != 0 || wrongDirectionFactor != 0)
				{
					anger += ((playerInfluence.length() / totalPlayerPush) + wrongDirectionFactor) * dt * MONSTER_ANGER_INCREMENT;
				}
				else if( anger > 0 )
				{
					anger -= MONSTER_COOLDOWN * dt;
				}

				//rage is now set, clamp
				anger = Math::clamp( anger, 1.5, 0 );

				speed = (randomDirection + playerInfluence) * (1.f + anger * 2.f);

				if( angryFaceTimer > 0 )
					angryFaceTimer -= dt;

				if( anger > 0.6 )
					angryFaceTimer = 1; //stay angry!

				if( angryFaceTimer > 0 && getAnimationIndex() != 1 )
				{
					setAnimation( 1 );
					Platform::getSingleton()->getSoundManager()->playSound(mLevel->getSound("uo"));
				}
				else if( angryFaceTimer <= 0 && getAnimationIndex() != 0 )
					setAnimation( 0 );

				animationSpeedMultiplier = (1.f + speed.length()*0.2f);

				color = Color::WHITE.lerp( anger, Color::RED );

				playerInfluence = 0;
				totalPlayerPush = 0;

				particleTimer += dt * animationSpeedMultiplier;
				if( particleTimer > 0.4 )
				{
					particleTimer = 0;
					Particle::emit(1, aabb.getCenter() + position - Vector(0,0.2f), mLevel );
				}
			}
			else if( isCurrentState( MS_HITTING ) )
			{
				//do a single cycle and end
				if( getAnimationElapsedLoops() > 0 )
					setState( MS_WALKING );
			}
			else if( isCurrentState( MS_DRINKING ) )
			{
				DEBUG_MESSAGE( animationSpeedMultiplier );

				if( getAnimationElapsedLoops() > 0 )
					setState( MS_WALKING );
			}
			else if( isCurrentState( MS_FALLING ) )
			{
				pixelScale -= 1.f * dt;
				rotation = Quaternion( Vector(0,0, timer*20.f) ); 
				timer += dt;

				if( !isFading() )
					setState( MS_DEAD );
			}			
		}

		virtual void onStateEnd()
		{
			if( isCurrentState( MS_HITTING) )
			{
				anger = Math::max( 0.f, anger - ANGER_HIT_DECREASE);
			}
		}
	private:

	protected:
	private:
	};
}

#endif // Monster_h__
