#ifndef Player_h__
#define Player_h__

#include "monster_common_header.h"

#include "Actor.h"
#include "Level.h"
#include "Item.h"
#include "Particle.h"

#define PLAYER_SPEED 110.f
#define PLAYER_SPEED_INFLUENCING (PLAYER_SPEED * 0.1f)
#define PLAYER_SPEED_CARRYING (PLAYER_SPEED * 0.5f)
#define THROW_FORCE 6.f
#define ATTRACT_TIME 0.5f
#define TUMBLE_TIME 0.3f
#define GRAVITY 30.f

namespace Monster
{
	class Player : 
		public Actor,
		public Dojo::StateInterface,
		public Dojo::InputDevice::Listener
	{
	public:

		enum State
		{
			PS_IDLE,
			PS_WALKING,
			PS_CARRYING_IDLE,
			PS_CARRYING,
			PS_ATTRACTING,
			PS_TUMBLING,
			PS_FALLING,
			PS_DYING
		};

		Player( Level* level, const Dojo::Vector& pos, Dojo::InputDevice* device, int ID ) :
		Actor( level, pos, "minion" + String(ID) + "_idle_right", 0.1 ),
		mDevice( device ),
		mLevel( level ),
		health( 3 ),
		damageTimer(0)
		{
			String prefix = "minion" + String(ID);
			registerAnimation( prefix + "_walk_right", 0.1 );
			registerAnimation( prefix + "_object_idle", 0.1 );
			registerAnimation( prefix + "_object_walk", 0.1 );
			registerAnimation( prefix + "_taunt", 0.2 );
			setAnimation(0);

			device->addListener( this );
	
			//setSize(getSize().x , getSize().y/2);

			aabb.max.x = getHalfSize().x * 0.7;
			aabb.max.y = 0.0f;

			aabb.min = -getHalfSize();
			aabb.min.x *= 0.7;

			base.max = Vector::ZERO;
			base.min = Vector::ZERO;
			base.translate( Vector( 0, -getHalfSize().x * 0.7 ) );

			/*base.max = aabb.max * 0.2f;
			base.min = aabb.min * 0.2f;*/

			/*light = new Sprite( level, aabb.getCenter(), "light" );
			light->destBlend = GL_ONE;
			light->srcBlend = GL_DST_COLOR;
			light->getTexture()->enableBilinearFiltering();
			light->pixelScale = 1.2;
			addChild( light, Level::LL_GUI1 );*/

			heartWidget = new Sprite( level, getHalfSize(), "cuori" );
			heartWidget->setVisible( false );
			addChild( heartWidget, Level::LL_GUI1 );

			setState( PS_IDLE );
		}

		bool isInControl()
		{
			return !isCurrentState( PS_DYING ) || isCurrentState( PS_FALLING ) || isCurrentState( PS_TUMBLING );
		}

		bool _heal()
		{
			if( health < 3 )
			{
				++health;
				heartWidget->setFrame( 3 - health );
				heartWidget->startFade( 1, 0, 1 );

				Platform::getSingleton()->getSoundManager()->playSound( mLevel->getSound("powerup") );

				return true;
			}
			else return false;
		}

		void _damage()
		{
			if( damageTimer > 1.f && health > 0 ) //it will be damaged only after 1 s passed from the last damage
			{
				--health;
				damageTimer = 0;

				Platform::getSingleton()->getSoundManager()->playSound( mLevel->getSound("hit") );

				if( health == 0 )
					setState( PS_DYING );
				else
				{			
					startFade( Color::RED, Color::WHITE, 0.2 );		
					heartWidget->setFrame( 3 - health );
					heartWidget->startFade( 1, 0, 1 );
				}
			}
		}

		virtual void onExplosion( const Vector& direction, float intensity )
		{
			if( isInControl() )
			{
				speed = direction * intensity * 60.f;
				speed.y += GRAVITY * TUMBLE_TIME; //fake a drop!
				setState( PS_TUMBLING );
			}
		}

		///ButtonPressed events are sent when the button bound to "action" is pressed on the device j
		virtual void onButtonPressed( Dojo::InputDevice* j, int action )	
		{
			if( isCurrentState( PS_WALKING ) || isCurrentState( PS_IDLE ) || isCurrentState( PS_ATTRACTING ) )
			{
				if( action == ACTION_ATTRACT )
				{
					setState( PS_ATTRACTING );

					Platform::getSingleton()->getSoundManager()->playSound(mLevel->getSound("insult"))->setVolume(0.5f);
				}
			}
			else if( isCurrentState( PS_CARRYING ) )
			{
				if( action == ACTION_ATTRACT )
					setState( PS_IDLE );
			}
		}
		///ButtonReleased events are sent when the button bound to "action" is released on the device j
		virtual void onButtonReleased( Dojo::InputDevice* j, int action )	
		{
			if( isCurrentState( PS_IDLE ) )
			{
				if( action == ACTION_ATTRACT )
					setState( PS_WALKING );
			}
		}

		///AxisMoved events are sent when the axis a is changed on the device j, with a current state of "state" and with the reported relative speed
		virtual void onAxisMoved( Dojo::InputDevice* j, Dojo::InputDevice::Axis a, float state, float speed )
		{

		}

		///this event is fired just before the device is disconnected and the InputDevice object deleted
		virtual void onDisconnected( Dojo::InputDevice* j ) 
		{
			mDevice = NULL;
			setState( PS_DYING );
		}

		virtual void onAction( float dt )
		{
			loop( dt );

			Sprite::onAction( dt );
		}

		virtual void onDestruction()
		{
			if( mDevice )
				mDevice->removeListener( this );
			mLevel->onPlayerDead( this );
		}

		AABB aabb;

	protected:

		Level* mLevel;
		Dojo::InputDevice* mDevice;
		Item* mItem;
		double timer;
		double damageTimer;
		Sprite* light;
		Sprite* heartWidget;

		int health;

		void _setSpeed( float module, float dt )
		{
			//todo collision
			if( mDevice )
			{
				if( mLevel->isCurrentState( Level::LS_DEBUG ) )
					module *= 3;

				speed.x += mDevice->getAxis( Dojo::InputDevice::AI_LX ) * dt * module;
				speed.y += mDevice->getAxis( Dojo::InputDevice::AI_LY ) * dt * module;
			}

			if( speed.x != 0 )
				pixelScale.x = Math::sign( speed.x );
		}

		bool _pickup()
		{
			//check for an item near
			mItem = mLevel->getItem( position );
			return mItem && mItem->onPickup();
		}


		virtual void onStateBegin()
		{
			if( isCurrentState( PS_IDLE ) )
			{
				setAnimation( 0 );
			}
			else if( isCurrentState( PS_WALKING ) )
			{
				setAnimation( 1 );
			}
			else if( isCurrentState( PS_ATTRACTING ) )
			{
				setAnimation( 4 );
				timer = 0;
			}
			else if( isCurrentState( PS_CARRYING_IDLE ) )
			{
				setAnimation( 2 );
			}
			else if( isCurrentState( PS_CARRYING ) )
			{
				setAnimation( 3 );
			}
			else if( isCurrentState( PS_TUMBLING ) )
			{
				setAnimation( 1 );
				startFade( Color::RED, Color::WHITE, TUMBLE_TIME );
				timer = 0;
			}
			else if( isCurrentState( PS_FALLING ) )
			{
				Platform::getSingleton()->getSoundManager()->playSound( mLevel->getSound("fall") );

				heartWidget->setVisible( false );
				setAnimation( 1 );
				startFade( Color::WHITE, Color::BLACK, 1 );
				timer = 0;
				speed *= 0.1;
			}
			else if( isCurrentState( PS_DYING ) )
			{
				startFade( Color::WHITE, Color::NIL, 1 );
				setAnimation( 0 );
				speed = 0;
				rotation = Quaternion( Vector(0,0,Math::PI/2.f));
			}
		}

		virtual void onStateLoop( float dt )
		{
			float dirX, dirY;
			//mLevel->collidesPlayer(this);
			int centerTileType;
			int tileType = mLevel->collidesActor(this, position+aabb.max, position+aabb.min, dirX, dirY, centerTileType );

			damageTimer += dt;

			if( !isCurrentState( PS_TUMBLING) && !isCurrentState( PS_FALLING ) )
			{
				speed = 0;

				if( tileType == 2 ) //spikes
				{
					_damage();
					_setSpeed( PLAYER_SPEED, dt ); //reapply speed to double it
				}
				else if( centerTileType == 3 )  //pits
					 setState( PS_FALLING );
		
				//find how much we're bouncing on other players
				for( auto player : mLevel->getPlayers() )
				{
					if( player == this )
						continue;

					if( position.distance(player->position) < getHalfSize().x )
						speed += (position-player->position).normalized();
				}

				//check if we are too near to the monster
				Monster* m = mLevel->getMonster();

				animationSpeedMultiplier = 1.f + m->getAnger();

				if( m->position.distance(position) < m->getSize().length() * 0.3 )
				{
					speed += (position - m->position)* 10.f;
					speed.y += GRAVITY * TUMBLE_TIME; //fake a drop!
					m->onPlayerHit();
					setState( PS_TUMBLING );
					return;
				}
			}

			if( isCurrentState( PS_IDLE ) )
			{
				_setSpeed( PLAYER_SPEED,dt );

				if( speed.length() > 0.05 )
					setState( PS_WALKING );
			}
			else if( isCurrentState( PS_WALKING ) )	
			{
				_setSpeed( PLAYER_SPEED, dt );
				if( _pickup() )
					setState( PS_CARRYING_IDLE );
				else if( speed.length() < 0.05 )
					setState( PS_IDLE );
			}
			else if( isCurrentState( PS_ATTRACTING ) )
			{
				_setSpeed( PLAYER_SPEED_INFLUENCING, dt );
				mLevel->getMonster()->onAttract( position, true );
				pixelScale.x = 1;

				timer += dt;
				//only last for an animation cycle
				if( timer > ATTRACT_TIME )
					setState( PS_IDLE );
			}
			else if( isCurrentState( PS_CARRYING_IDLE ) )
			{
				_setSpeed( PLAYER_SPEED_CARRYING, dt );
				if( speed.length() > 0.05 )
					setState( PS_CARRYING );
			}
			else if( isCurrentState( PS_CARRYING ) )
			{
				_setSpeed( PLAYER_SPEED_CARRYING, dt );
				if(mItem) mItem->position = position + Vector( 0, 0.5 );
				if( speed.length() < 0.05 )
					setState( PS_CARRYING_IDLE );
			}
			else if( isCurrentState( PS_TUMBLING ) )
			{
				speed.y -= GRAVITY * dt;
				rotation = Quaternion( Vector(0,0, timer*20.f) ); 
				timer += dt;
				if( timer > TUMBLE_TIME )
				{
					_damage();
					if( health > 0 )
						setState( PS_IDLE );
				}
			}
			else if( isCurrentState( PS_FALLING ) )
			{
				pixelScale -= 1.f * dt;
				rotation = Quaternion( Vector(0,0, timer*20.f) ); 
				timer += dt;
				if( !isFading() )
					dispose = true;
			}
			else if( isCurrentState( PS_DYING ) )
			{
				speed = 0;
				if( !isFading() )
					dispose = true;
			}

			//int tileCol = position.x / mLevel->getSize().x;
		}

		virtual void onStateEnd()
		{
			if( isCurrentState( PS_CARRYING ) || isCurrentState( PS_CARRYING_IDLE ) )
			{
				if( nextState != PS_CARRYING && nextState != PS_CARRYING_IDLE )
				{
					if( nextState == PS_TUMBLING )
						mItem->onDrop();
					else
						mItem->onThrow( speed * THROW_FORCE );
					mItem = NULL;
				}
			}
			if( isCurrentState( PS_TUMBLING ) )
			{
				rotation = Quaternion(); //identity
				speed = 0;

				Particle::emit( 10, position, mLevel );
			}
		}
	private:
	};
}
#endif // Player_h__
