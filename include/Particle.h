#ifndef PARTICLES_H__
#define PARTICLES_H__

#include "monster_common_header.h"

#include "Actor.h"

namespace Monster
{
	class Level;

	class Particle : public Actor
	{
	public:
		Particle(Level* level, const Dojo::Vector& pos, const String& name) : 
			Actor(level, pos, name)
		{
			color.a = 0.8;
			mLevel = level;
		}

		void onEmit(const Vector& launchSpeed, float life)
		{
			speed = launchSpeed;
			lifeTime = life;
		}

		virtual void onAction(float dt)
		{
			pixelScale -= dt;

			speed *= 0.95;

			lifeTime -= dt;
			if(lifeTime <= 0.0f)
			{
				//destroy - decrease alpha

				color.a -= dt*3.0f;
				if(color.a <= 0.0f)
				{
					color.a = 0.0f;
					dispose = true;
				}
			}

			Sprite::onAction( dt );
		}

		static void emit(int n, const Vector& pos, Level* level)
		{
			for(int i=0; i < n; i++)
			{
				Particle* part = new Particle(level, pos, "smoke");
				float angle = Math::random()*2.0f*3.14f;
				Vector dir(cos(angle), sin(angle));
				part->onEmit(dir.normalized() * (1+Math::random()*1.0f) , 0.5f);
				level->addChild(part, Level::LL_ACTORS);
			}
		}

		static void explosion(int n, const Vector& pos, Level* level)
		{
			for(int i=0; i < n; i++)
			{
				Particle* part = new Particle(level, pos, "smoke");
				float angle = Math::random()*2.0f*3.14f;
				Vector dir(cos(angle), sin(angle));
				part->onEmit(dir.normalized() * (1+Math::random()*1.0f) *3, 0.5f);
				part->startFade( 0xffffd200, Color::WHITE, 0.6 );
				part->pixelScale = 2;
				level->addChild(part, Level::LL_ACTORS);
			}
		}

	private:
		float lifeTime;
		Level* mLevel;
	};
	
}

#endif // PARTICLES_H__
