#ifndef Beer_h__
#define Beer_h__

#include "monster_common_header.h"

#include "Item.h"

namespace Monster
{
	class Beer : public Item
	{
	public:

		Beer( Level* l, const Vector& pos ) :
		Item( l, pos, "beer" )
		{

		}

		virtual void onStateLoop( float dt )
		{
			Monster* m = ((Level*)getGameState())->getMonster();

			m->onAttract( position, false, 0.3 );
			if( isCurrentState( IS_THROWN ) )
			{
				//check if the monster has been hit, and keep it angered!
				if( m->position.distance(shadow->position) < 1.2 )
				{
					m->setState( Monster::MS_DRINKING ); //HACK
					m->addAnger( -0.7 );

					setState( IS_DESTROYED );
				}
			}
			else if( isCurrentState( IS_IDLE ) )
			{
				//be picked up by the monster
				if( m->position.distance(shadow->position) < 1.2 )
				{
					m->setState( Monster::MS_DRINKING ); //HACK
					m->addAnger( -0.7 );

					setState( IS_DESTROYED );
				}
			}

			Item::onStateLoop( dt );
		}
	protected:
	private:
	};
}
#endif // Beer_h__
