#include "stdafx.h"

#include "monster_common_header.h"

#include "MonsterGame.h"

using namespace Dojo;
using namespace Monster;

int main( int argc, char** argv )
{
	Table config;
	//config.setBoolean( "fullscreen", true );

	Platform* platform = Platform::create( config );

	platform->run( new MonsterGame() );
	
	return 0;
}

#ifdef WIN32

INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT )
{
	main( 1, &strCmdLine );
}

#endif