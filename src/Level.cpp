#include "stdafx.h"

#include "Level.h"

#include "MonsterGame.h"
#include "Monster.h"
#include "Player.h"
#include "Room.h"

#include <functional>

using namespace Dojo;
using namespace Monster;

Level::Level( Dojo::Game* game) :
GameState( game ),
timeSpeed( 1 ),
tileSide( 0 )
{    

}

Level::~Level()
{
	
}

Player* Level::getNearestPlayer( const Vector & pos, float maxDixt )
{
	Player* found = NULL;
	float minDist = maxDixt;
	for( auto player : players )
	{
		float d = pos.distance( player->position );
		if( d < minDist )
		{
			found = player;
			minDist = d;
		}
	}
	return found;
}

Item* Level::getItem( const Vector& pos )
{
	for( auto& r : rooms )
	{
		Item* i = r->getItem( pos );
		if( i )
			return i;
	}
	return nullptr;
}

int Level::_getDescMask( Table* desc )
{
	int mask = 0;

	if( desc->getBool( "north") )	mask |= 0x1;
	if( desc->getBool( "east") )	mask |= 0x1 << 1;
	if( desc->getBool( "south") )	mask |= 0x1 << 2;
	if( desc->getBool( "west") )		mask |= 0x1 << 3;
	return mask;
}

int Level::_getCellAt( int j, int i )
{
	if( j >= mapWidth || i >= mapHeight || j < 0 || i < 0 )
		return 0;
	return roomMap[ j + i * mapWidth ];
}

int Level::_roomMaskAt( int j, int i )
{
	//check the 4 adjacent tiles
	int mask = 0;

	if( _getCellAt( j,i+1 ) )	mask |= 0x1;
	if( _getCellAt( j+1,i ) )	mask |= 0x1 << 1;
	if( _getCellAt( j,i-1 ) )	mask |= 0x1 << 2;
	if( _getCellAt( j-1,i ) )	mask |= 0x1 << 3;

	return mask;
}

Vector Level::_getWorldPositionForCell( int j, int i )
{
	Vector cellsize;
	camera->makeScreenSize( cellsize, 960, 960 );
	return Vector( j * cellsize.x, i * cellsize.y );
}

Room* Level::_randomRoomFor( int j, int i )
{
	//return all the rooms for this mask
	int mask = _roomMaskAt( j, i );
	if( mask == 0 )
		return nullptr;

	auto& fittingRoomDescs = roomDescriptions[ mask ];

	DEBUG_ASSERT( fittingRoomDescs.size(), "No room descriptions were loaded" );

	//choose a random table and create the room
	Table* desc = fittingRoomDescs[ Math::rangeRandom( 0, fittingRoomDescs.size()-1 ) ];

	return new Room( this, _getWorldPositionForCell(j,i), desc->getName(), _getCellAt(j,i) );
}

void Level::onBegin()
{
	addSubgroup( (MonsterGame*)getGame() );

	Math::seedRandom( (unsigned int)(this) );

	//create camera
	float r = (float)Platform::getSingleton()->getWindowWidth() / (float)Platform::getSingleton()->getWindowHeight();

	Vector cameraSize;
	cameraSize.x = 11.25 * r;
	cameraSize.y = 11.25;
	{
		camera = new Viewport(this, 
							  Vector(0,0,1), 
							  cameraSize,
							  0xff8d8771, 
							  70, 0.01, 1000 );

		camera->setTargetSize( Vector( 1080*r, 1080 ) );
		
		addChild( camera );
		setViewport( camera );

		Vector cellsize;
		camera->makeScreenSize( cellsize, 96, 96 );
		tileSide = cellsize.x;
	}

	//load map descriptions
	std::vector< String > roomNames;
	Platform::getSingleton()->getFilePathsForType( "ds", "data/rooms", roomNames );
	for( auto& roomPath : roomNames )
	{
		Table* desc = getTable( Utils::getFileName(roomPath) );
		int mask = _getDescMask( desc );
		roomDescriptions[mask].push_back( desc );
	}

	//generate the room map
	//HACK load from file
	Table* mapCollection = getTable( "map" );

	//load a random table
	int mapn =  Math::rangeRandom(0, mapCollection->getAutoMembers() );
	DEBUG_MESSAGE( "BEHOLD MAP "<< mapn );

	Table* map = mapCollection->getTable( mapn );

	//get table size
	mapHeight = map->getAutoMembers();
	mapWidth = map->getTable(0)->getAutoMembers();
	roomMap = new int[ mapWidth * mapHeight ];
	memset( roomMap, 0, sizeof(int) * mapWidth * mapHeight );

	bool invertMap = Math::random() > 0.5;
	int endType = invertMap ? 2 : 3;
	int spawnType = invertMap ? 3 : 2;

	for( int i = 0; i < mapHeight; ++i )
	{
		Table* row = map->getTable( i );
		for( int j = 0; j < mapWidth; ++j )
		{
			int cellType = row->getInt(j);
			roomMap[ j + i * mapWidth ] = cellType;

			if( cellType == spawnType )
				spawnPoint = _getWorldPositionForCell(j,i);
			else if( cellType == endType ) //create bed
			{
				bed = new Actor( this, _getWorldPositionForCell(j,i), "bed" );
				addChild( bed, LL_ACTORS );
			}
		}
	}

	//now, for each map cell, find a fitting room
	for( int i = 0; i < mapHeight; ++i )
	{
		Table* row = map->getTable( i );
		for( int j = 0; j < mapWidth; ++j )
		{
			if( _getCellAt(j,i) ) //if a real cell
			{
				Room* r = _randomRoomFor( j,i );
				if( r )
				{
					rooms.add( r );
					addChild( r, LL_BACKGROUND );
				}
			}
		}
	}

	monster = new Monster( this, spawnPoint );
	addChild( monster, LL_ACTORS );

	vignette = new Renderable( this, Vector::ZERO );
	vignette->setMesh( getMesh( "texturedQuad") );
	vignette->setTexture( getFrameSet( "vignette" )->getFrame(0) );
	vignette->setBlending( RenderState::BM_MULTIPLY );
	vignette->scale = cameraSize;
	camera->addChild( vignette, LL_GUI1 );

	vignette->startFade( Color::BLACK, Color::WHITE, 2 );

	//MUSIC
	goblinSound = Platform::getSingleton()->getSoundManager()->playSound(getSound("goblinking"));
	goblinSound->setLooping(true);
	goblinSound->setVolume(1.0f);

	cannibalSound = Platform::getSingleton()->getSoundManager()->playSound(getSound("timetokill"));
	cannibalSound->setLooping(true);
	cannibalSound->setVolume(1.0f - goblinSound->getVolume());

	//spawn already registered players
	auto devices = ((MonsterGame*)getGame())->getActiveDevices();
	for( auto j : devices )
		onPlayerConnected( j );

	setState( LS_GAMEPLAY );
}

void Level::setMusicMix(float vol)
{
	vol = Math::clamp(vol, 1.0f, 0.0f);
	goblinSound->setVolume(1.0f-vol);
	cannibalSound->setVolume(vol);
}

void Level::onLoop( float dt )
{
	dt *= timeSpeed;

	GameState::onLoop( dt );

	//sort on y!
	Render::Layer& layer = *Platform::getSingleton()->getRender()->getLayer( LL_ACTORS );
	std::multimap< float, Renderable*, std::greater<float> > sorted;
	for( auto actor : layer )
		sorted.insert( std::pair<float,Renderable* >( ((Actor*)actor)->getPivot().y, actor ) );

	//clear layer and re-add
	layer.clear();
	for( auto actor : sorted )
		layer.add( actor.second );
}

void Level::onEnd()
{
	//HACK remove all the stuff from the render - this should happen automatically!
	Platform::getSingleton()->getRender()->removeAllRenderables();

	goblinSound->stop();
	cannibalSound->stop();
}

void Level::onPlayerDead( Player* p )
{
	players.remove( p );

	if( players.size() == 0 )
		setState( LS_GAMEOVER );
}

void Level::onStateBegin()
{
	if( isCurrentState( LS_GAMEPLAY ) )
	{

	}
	else if( isCurrentState( LS_GAMEOVER ) )
	{
		vignette->setTexture( getFrameSet( "gameover" )->getFrame(0) );
		vignette->color = Color::WHITE;
		vignette->destBlend = GL_ZERO;
		vignette->srcBlend = GL_ONE;
		timeSpeed = 0.01;
	}
	else if( isCurrentState( LS_WIN ) )
	{
		vignette->setTexture( getFrameSet( "winscreen" )->getFrame(0) );
		vignette->color = Color::WHITE;
		vignette->destBlend = GL_ZERO;
		vignette->srcBlend = GL_ONE;
		timeSpeed = 0.01;
	}
}

void Level::onStateLoop( float dt )
{
	if( isCurrentState( LS_GAMEPLAY ) )
	{
		camera->position = monster->position;
		camera->position.z = 1;

		if( monster->isCurrentState( Monster::MS_DEAD) )
			setState( LS_GAMEOVER );
		if( monster->position.distance( bed->position) < 0.5 )
			setState( LS_WIN );
	}
	else if( isCurrentState( LS_DEBUG) )
	{
		camera->position = players[0]->position * 0.5;
		camera->position.z = 1;
	}
}

void Level::onStateEnd()
{

}

void Level::onPlayerConnected( Dojo::InputDevice* j )
{	
	if( players.size() >= MAX_PLAYERS )
		return; //no more than 4 players

	float angle = (Math::TAU / (float)MAX_PLAYERS) * players.size();
	Vector pos = monster->position + (Vector::UNIT_Y * Quaternion( Vector(0,0,angle ) ) * 1.5f );
	Player* p = new Player( this, pos, j, players.size()+1 );
	addChild( p, LL_ACTORS );
	players.add( p );
}

bool Level::collides(const Vector& max, const Vector& min)
{
	bool colliding = false;

	Room* room = 0;
	for( auto room : rooms )
	{
		colliding = room->collides(max, min);
		if(colliding)
			return true;
	}

	return colliding;
}

void Level::debugAABBs()
{
	for(int i=0; i < rooms.size(); i++)
	{
		Room* r = rooms[i];

		for(int j=0; j < r->aabbs.size(); j++)
		{
			const Tile& boundingBox = r->aabbs[j];
			const Vector& maxB = boundingBox.max;
			const Vector& minB = boundingBox.min;

			Renderable* r = new Renderable( this, (minB + maxB)*0.5f );
			r->setMesh( getMesh("wireframeQuad") );
			r->scale = maxB - minB ;//Vector(txw*2,tyw*2);
			addChild( r, LL_GUI1 );
		}
	}
}

int Level::collidesActor(Actor* p, const Vector& maxA, const Vector& minA, float& dirX, float& dirY, int& centerTile)
{
	Room* room = 0;
	centerTile = -1;
	AABB base = p->base;
	base.translate( p->position );

	int type = -1;

	for(int i=0; i < rooms.size(); i++)
	{
		room = rooms[i];

		for(int j=0; j < room->aabbs.size(); j++)
		{
			const Tile& boundingBox = room->aabbs[j];
			auto& maxB = boundingBox.max;
			auto& minB = boundingBox.min;

			//check if this tile contains the actor's pivot (if not already found)
			if( centerTile == -1 && Math::AABBContainsAABB( maxB, minB, base.max, base.min ) )
				centerTile = boundingBox.type;

			float posX= minA.x+((maxA.x-minA.x)/2);
			float posY= minA.y+((maxA.y-minA.y)/2);

			float tX = minB.x+((maxB.x-minB.x)/2);
			float tY = minB.y+((maxB.y-minB.y)/2);

			float txw = ((maxB.x-minB.x)/2);
			float tyw = ((maxB.y-minB.y)/2);

			float objhw = ((maxA.x-minA.x)/2);
			float objhh = ((maxA.y-minA.y)/2);

			float dx = posX - tX;//tile->obj delta
			float px = (txw + objhw) - Math::abs(dx);//penetration depth in x

			//Renderable* r = new Renderable( this, (minB + maxB)*0.5f );
			//r->setMesh( getMesh("wireframeQuad") );
			//r->scale = maxB - minB ;//Vector(txw*2,tyw*2);
			//addChild( r, LL_GUI1 );

			if(px > 0 )
			{
				float dy = posY - tY; //tile->obj delta
				float py = (tyw + objhh) - Math::abs(dy);//pen depth in y
				
				if(py > 0)
				{
					//object may be colliding with tile
					
					//calculate projection vectors
					if(px < py)
					{
						//entity.speed.x = 0;
						//project in x
						if(dx < 0)
						{
							//project to the left
							px *= -1;
							py = 0;
						}
						else
						{
							//proj to right
							py = 0;
						}
					}
					else //px > py
					{				
						//entity.speed.y = 0;
						//project in y
						if(dy < 0)
						{
							//project up
							px = 0;
							py *= -1;
						}
						else
						{
							//project down
							px = 0;
						}
					}
					
					if( boundingBox.type == 1 )
					{
						p->position.x += px;
						p->position.y += py;
					}
	
					type = boundingBox.type;	
				}
			}
		}
	}

	return type;
}
