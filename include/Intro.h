#ifndef Intro_h__
#define Intro_h__

#include "monster_common_header.h"

#include "MonsterGame.h"

namespace Monster
{
	class Intro : 
		public GameState
	{
	public:

		enum State
		{
			LS_PRESS_START,
			LS_PLAYER_CONNECT
		};

		Intro( Game* game ) :
		GameState( game ),
		connected( 0 )
		{

		}
		
		virtual void onPlayerConnected( InputDevice* j )
		{
			if( isCurrentState( LS_PLAYER_CONNECT ) )
			{
				if( connected == 0 )
					minions->setVisible( true );
				else 
					minions->setFrame( connected );

				Platform::getSingleton()->getSoundManager()->playSound( getSound("insult") )->setVolume( 0.5 );
				++connected;
			}
		}

		virtual void onButtonPressed( int action )
		{
			if( isCurrentState( LS_PRESS_START ) )
				setState( LS_PLAYER_CONNECT );
			else if( isCurrentState( LS_PLAYER_CONNECT ) && action == ACTION_START && connected >= 1 )
				((MonsterGame*)getGame())->reset();
		}

	protected:

		Sprite* screen, *minions;

		int connected;

		//------ state events
		virtual void onBegin()
		{
			addSubgroup(((MonsterGame*)getGame()));
			addFolder( "data/intro" );
			loadResources();

			float r = (float)Platform::getSingleton()->getWindowWidth() / (float)Platform::getSingleton()->getWindowHeight();
			Vector cameraSize;
			cameraSize.x = 11.25 * r;
			cameraSize.y = 11.25;
			{
				camera = new Viewport(this, 
					Vector(0,0,1), 
					cameraSize,
					0xff8d8771, 
					70, 0.01, 1000 );

				camera->setTargetSize( Vector( game->getNativeHeight()*r, game->getNativeHeight() ) );

				addChild( camera );
				setViewport( camera );
			}

			minions = new Sprite( this, Vector( 0, -5 ), "heads" );
			minions->setVisible(false);
			addChild( minions, 2 );

			Platform::getSingleton()->getSoundManager()->playMusic( getSound( "goblinking" ) );

			setState( LS_PRESS_START );
		}

		virtual void onEnd()
		{
			Platform::getSingleton()->getSoundManager()->stopMusic();
		}

		//----- immediate substate events
		virtual void onStateBegin()
		{
			if( isCurrentState( LS_PRESS_START ) )
			{
				screen = new Sprite( this, Vector::ZERO, "startscreen" );
				addChild( screen, 1 );
			}
			else if( isCurrentState( LS_PLAYER_CONNECT ) )
			{
				screen->dispose = true;
				screen = new Sprite( this, Vector::ZERO, "introscreen" );
				addChild( screen, 1 );

				//spawn already registered players
				auto devices = ((MonsterGame*)getGame())->getActiveDevices();
				for( auto j : devices )
					onPlayerConnected( j );
			}
		}

		virtual void onStateLoop( float dt )
		{

		}

		virtual void onStateEnd()
		{

		}

	private:
	};
}

#endif // Intro_h__
