#ifndef ROOM_H__
#define ROOM_H__

#include "monster_common_header.h"
#include "Level.h"
#include <vector>

#include "Beer.h"
#include "Stone.h"
#include "Bomb.h"
#include "Saw.h"
#include "Heart.h"

#define TILE_SIZE 1
#define TILE_MULT 0.25

#define ITEM_PICKUP_RADIUS 0.5

namespace Monster
{
	class Room : 
		public Dojo::Sprite
	{
	public:

		Room(Level* level, const Dojo::Vector& pos, const String& name, int roomType):
			Sprite(level, pos, name)
		{
			mLevel = level;
			Table *table = level->getTable(name);
			Table* room = table->getTable("room");
			
			Vector tileSize = getSize() / 10.f; //tile numbers
			Vector topCorner = position + Vector( -getHalfSize().x, getHalfSize().y );

			for(int i=0; i < room->getAutoMembers(); i++)
			{
				Table* row = room->getTable(i);

				for(int j=0; j < row->getAutoMembers(); j++)
				{
					int tile = row->getInt(j);
						
					if(tile != 0)
					{

						Vector topLeft = topCorner + Vector( (j+1) * tileSize.x, -(i+1) * tileSize.y );
						Vector bottomRight = topLeft - Vector( tileSize.x, - tileSize.y ) ;

						Vector max( Math::max( topLeft.x, bottomRight.x ), Math::max( topLeft.y, bottomRight.y) );
						Vector min( Math::min( topLeft.x, bottomRight.x ), Math::min( topLeft.y, bottomRight.y) );

						aabbs.push_back( Level::Tile( max, min, tile ) );

						//special tiles
						if( tile == 4 ) //saw
						{
							Saw* s = new Saw( level, (max+min)*0.5f );
							level->addChild( s, Level::LL_ACTORS );
						}
					}
					else if(tile == 0 && roomType == 1 )
					{
						if(Math::random() < 0.02f)
						{
							Vector topLeft = topCorner + Vector( (j+1) * tileSize.x, -(i+1) * tileSize.y );
							Vector bottomRight = topLeft - Vector( tileSize.x, - tileSize.y ) ;
							//Vector itemPos((j+1) * tileSize.x, -(i+1) * tileSize.y);

							Item* i = 0;
							Vector pos = topLeft + (bottomRight-topLeft)*0.5f;
							float r = Math::random();

							if(r < 0.1 )
								i = new Bomb( level, pos );
							else if( r < 0.3 )
								i = new Heart( level, pos );
							else if( r < 0.7 )
								i = new Stone( level, pos );
							else
								i = new Beer( level, pos );
						
							items.add( i );
							level->addChild( i, Level::LL_ACTORS );
						}
					}
				}
			}
		}

		bool collides( const Vector& maxA, const Vector& minA)
		{
			for( auto& boundingBox : aabbs )
			{
				const Vector& maxB = boundingBox.max;
				const Vector& minB = boundingBox.min;

				if(Math::AABBsCollide(maxA, minA, maxB, minB))
					return true;
			}

			return false;
		}

		Item* getItem( const Vector& pos )
		{
			//find an item near enough
			float minDist = ITEM_PICKUP_RADIUS;
			Item* i = nullptr;
			for( auto item : items )
			{
				if( !item->canPickUp() )
					continue;

				float d = item->position.distance( pos );
				if( d < minDist )
				{
					i = item;
					minDist = d;
				}
			}
			return i;
		}


		std::vector<Level::Tile> aabbs;

	private:
		Level* mLevel;

		Array< Item* > items;
	};
}

#endif // ROOM_H__