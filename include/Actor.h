#ifndef Actor_h__
#define Actor_h__

#include "monster_common_header.h"

namespace Monster
{
	class Level;

	struct AABB
	{
		Vector max;
		Vector min;

		void translate( const Vector& t )
		{
			max += t;
			min += t;
		}

		void scale( const Vector& components )
		{
			max.mulComponents( components );
			min.mulComponents( components );
		}

		Vector getCenter()
		{
			return (max+min)*0.5f;
		}
	};

	class Actor : public Sprite
	{
	public:

		AABB aabb, base;

		Actor( Level * level, const Dojo::Vector& pos, const String& string, float dt = 0.1 ) :
		Sprite( level, pos, string, dt )
		{
			aabb.max = getHalfSize();
			aabb.min = -getHalfSize();

			base = aabb;
		}

		Vector getPivot()
		{
			return aabb.getCenter() + position;
		}

		virtual void onExplosion( const Vector& direction, float intensity )
		{

		}

	protected:
	private:
	};
}

#endif // Actor_h__
