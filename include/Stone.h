#ifndef Stone_h__
#define Stone_h__

#include "monster_common_header.h"

#include "Item.h"

namespace Monster
{
	class Stone : public Item
	{
	public:

		Stone( Level* l, const Vector& pos ) :
			Item( l, pos, "rock" )
		{

		}

		virtual void onStateLoop( float dt )
		{
			Monster* m = ((Level*)getGameState())->getMonster();

			if( isCurrentState( IS_DESTROYED_ATTRACTING ) ) //keep the thing angered
			{
				m->onAttract( playerPos, true, 2 );

				timer += dt;
				if( timer > 0.5 )
					setState( IS_DESTROYED );
			}
			else if( isCurrentState( IS_THROWN ) )
			{
				//check if the monster has been hit, and keep it angered!
				if( m->position.distance(shadow->position) < 1.2 )
				{
					m->addAnger( -0.5 );
					setState( IS_DESTROYED_ATTRACTING );
				}
			}

			Item::onStateLoop( dt );
		}
	protected:
	private:
	};
}
#endif // Beer_h__
