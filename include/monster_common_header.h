//
//  ft_common_header.h
//  Future Tactics
//
//  Created by Tommaso Checchi on 8/9/11.
//  Copyright 2011 none. All rights reserved.
//

#ifndef oo_common_header_h
#define oo_common_header_h

#include <dojo.h>

#include <set>

using namespace Dojo;

#define MAX_PLAYERS 4 

#define ACTION_ATTRACT 1
#define ACTION_SCARE 2
#define ACTION_RESET 3
#define ACTION_START 4

#endif 