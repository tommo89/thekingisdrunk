#include "stdafx.h"

#include "MonsterGame.h"
#include "Level.h"
#include "Intro.h"

using namespace Monster;
using namespace Dojo;

MonsterGame::MonsterGame() : 
Game( 
	 "The King is Drunk",
	 1920, 1080,
	 DO_LANDSCAPE_RIGHT ),
currentLevel( nullptr ),
currentIntro( nullptr )
{
	
}

MonsterGame::~MonsterGame()
{
	
}

void MonsterGame::reset()
{
	currentIntro = nullptr;
	currentLevel = new Level( this );

	setState( currentLevel );
}

void MonsterGame::onBegin()
{
	//load resources
	addPrefabMeshes();
	addFolder( "data" );
	addFolder( "data/rooms" );
	addFolder( "data/sounds" );
	loadResources();

	Platform::getSingleton()->getInput()->addListener( this );

	//and get all the already connected devices
	auto devices = Platform::getSingleton()->getInput()->getDeviceList();
	for( auto j : devices )
		onDeviceConnected( j );

	//go with the intro
	currentIntro = new Intro( this );
	setState( currentIntro );
}

void MonsterGame::onLoop( float dt )
{

}

void MonsterGame::onEnd()
{
	Platform::getSingleton()->getInput()->removeListener( this );
}


///------------------------------------------------------------------

void MonsterGame::onStateBegin()
{
	
}

void MonsterGame::onStateLoop( float dt )
{
	
}

void MonsterGame::onStateEnd()
{
	
}

///------------------------------------------------------------------

void MonsterGame::onDeviceConnected( Dojo::InputDevice* j )
{
	//listen on all the joysticks
	j->addListener( this );

	if( j->getType() == String("keyboard") )
	{
		Keyboard* k = (Keyboard*)j;

		j->addBinding( ACTION_ATTRACT, KC_X );
		j->addBinding( ACTION_SCARE, KC_Z );
		j->addBinding( ACTION_RESET, KC_R );
		j->addBinding( ACTION_START, KC_RETURN );

		k->addFakeAxis( InputDevice::AI_LX, KC_LEFT, KC_RIGHT );
		k->addFakeAxis( InputDevice::AI_LY, KC_DOWN, KC_UP );
	}
	else
	{
		j->addBinding( ACTION_ATTRACT, KC_JOYPAD_13 );
		j->addBinding( ACTION_SCARE, KC_JOYPAD_15 );
		j->addBinding( ACTION_RESET, KC_JOYPAD_6 );
		j->addBinding( ACTION_START, KC_JOYPAD_5 );
	}
}

void MonsterGame::onDeviceDisconnected( Dojo::InputDevice* j )
{
	activeDevices.remove( j );
}

void MonsterGame::onButtonPressed( Dojo::InputDevice* j, int action )
{
	if( currentIntro )
		currentIntro->onButtonPressed( action ); //pressed ANY FUCKIN KEY

	if( action == KC_F1 )
	{
		if( currentLevel )
			currentLevel->setState( Level::LS_DEBUG );
	}
	else if( action == ACTION_RESET )
		reset();

	else if( action == ACTION_ATTRACT && !activeDevices.exists( j ) )
	{
		activeDevices.add( j );

		if( currentLevel )
			currentLevel->onPlayerConnected( j );
		else if( currentIntro )
			currentIntro->onPlayerConnected( j );
	}
}
