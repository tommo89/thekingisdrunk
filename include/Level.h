//
//  Map.h
//  Future Tactics
//
//  Created by Tommaso Checchi on 8/10/11.
//  Copyright 2011 none. All rights reserved.
//

#ifndef LEVEL_H__
#define LEVEL_H__

#include "monster_common_header.h"

//#include "Room.h"

namespace Monster
{
	class Item;
	class MonsterGame;
	class Player;
	class Monster;
	class Room;
	class Actor;

	class Level : 
		public Dojo::GameState
	{
	public:

		struct Tile
		{
			Vector max, min;
			int type;

			Tile( const Vector& M, const Vector& m, int t ) :
			max( M ), 
			min( m ),
			type( t )
			{

			}
		};

		enum State
		{
			LS_GAMEPLAY,
			LS_WIN,
			LS_GAMEOVER,
			LS_DEBUG,
		};
				
		enum Layer
		{
			LL_BACKGROUND,
			LL_SHADOWS,
			LL_ACTORS,
			LL_OVERLAY,
			LL_GUI1,
			LL_GUI2,

			LL_VIGNETTE,

			LL_FADER
		};

		Level( Dojo::Game* game );
		
		virtual ~Level();

		inline Monster* getMonster()
		{
			return monster;
		}

		Item* getItem( const Vector & pos );

		Player* getNearestPlayer(  const Vector & pos, float maxDixt );

		Array<Player*>& getPlayers()
		{
			return players;
		}

		float getTileSide()
		{
			return tileSide;
		}

		bool collides( const Vector& max, const Vector& min );

		int collidesActor(Actor* p, const Vector& max, const Vector& min, float& dirX, float& dirY, int & centerTile);
		//void collidesPlayer(Player* p);

		void debugAABBs();

		virtual void onPlayerConnected( Dojo::InputDevice* d );

		void onPlayerDead( Player* p );

		void setMusicMix(float vol);
				
	protected:
		
		//SoundManager* soundManager;
		Monster* monster;
		Array< Player* > players;
		Array<Room*> rooms;
		Array< Item* > items;
		std::unordered_map< int, std::vector<Table*> > roomDescriptions;
		Renderable* vignette;
		int *roomMap;
		int mapWidth, mapHeight;
		SoundSource* goblinSound;
		SoundSource* cannibalSound;

		float timeSpeed;
		float tileSide;

		Vector spawnPoint;
		Sprite* bed;

		virtual void onBegin();		
		virtual void onLoop( float dt );		
		virtual void onEnd();

		virtual void onStateBegin();
		virtual void onStateLoop( float dt );
		virtual void onStateEnd();


		int _getDescMask( Table* desc );

		int _getCellAt( int j, int i );
		int _roomMaskAt( int j, int i );
		Room* _randomRoomFor( int j, int i );
		Vector _getWorldPositionForCell( int j, int i );
	};
}

#endif
