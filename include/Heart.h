#ifndef Hearth_h__
#define Hearth_h__

#include "monster_common_header.h"

#include "Item.h"

namespace Monster
{
	class Heart : public Item
	{
	public:

		Heart( Level* l, const Vector& pos ) :
			Item( l, pos, "heart" )
		{

		}

		virtual void onStateLoop( float dt )
		{
			if( isCurrentState( IS_IDLE ) )
			{
				Player* p = mLevel->getNearestPlayer( position, .7 );
				if( p && p->_heal() )
					setState( IS_DESTROYED );
			}

			Item::onStateLoop( dt );
		}
	protected:
	private:
	};
}
#endif // Beer_h__
