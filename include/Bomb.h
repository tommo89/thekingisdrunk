#ifndef Bomb_h__
#define Bomb_h__	

#include "monster_common_header.h"

#include "Item.h"

#define BOMB_RADIUS	3.f

namespace Monster
{
	class Bomb : public Item
	{
	public:

		Bomb( Level* l, const Vector& pos ) :
			Item( l, pos, "bomb" ),
			explode( false )
		{

		}

		virtual void onExplosion( const Vector& direction, float intensity )
		{
			if( !isCurrentState( IS_DESTROYED) )
				setState( IS_DESTROYED );
		}

		virtual void onStateBegin() 
		{
			//if it has been destroyed, EXPLODE
			if( isCurrentState( IS_DESTROYED ) )
			{
				_explode();
			}

			Item::onStateBegin();
		}

		virtual bool onTransition()
		{
			if( isCurrentState(IS_THROWN) && nextState == IS_IDLE )
				explode = true; //schedule explosion
			return true;
		}

		virtual void onStateLoop( float dt )
		{
			Monster* m = ((Level*)getGameState())->getMonster();
			m->onAttract( position, false, 0.05 );
			if( isCurrentState( IS_THROWN ) )
			{
				if( m->position.distance(shadow->position) < 1.2 )
					setState( IS_DESTROYED );
			}
			else if( isCurrentState( IS_IDLE ) )
			{
				//check if the monster hits the bomb
				if( explode || m->position.distance(shadow->position) < 1.2 )
					setState( IS_DESTROYED );
			}

			Item::onStateLoop( dt );
		}

	protected:

		bool explode;
		void _explode()
		{
			Platform::getSingleton()->getSoundManager()->playSound( mLevel->getSound( "explosion" ) ); 
			Particle::explosion( 30, position, mLevel );

			if( Math::random() < 0.3 )
			{
				startFade( Color::BLACK, Color::WHITE, 0.5 );
				return;
			}

			//check the objects
			Render::Layer* layer = Platform::getSingleton()->getRender()->getLayer( Level::LL_ACTORS );

			for( auto actor : *layer )
			{
				if( actor == this )
					continue;

				Vector dir = ((Actor*)actor)->getPivot() - position;
				float d = dir.length();
				if( d < BOMB_RADIUS )
					((Actor*)actor)->onExplosion( dir.normalized(), d );
			}
		}
	private:
	};
}
#endif // Beer_h__
